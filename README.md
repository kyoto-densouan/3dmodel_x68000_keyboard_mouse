# README #

1/3スケールのSHARP X68000用キーボード・マウス風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_x68000_keyboard_mouse/raw/7804e2cb25bb556c4f7c206e6624d596610cb27c/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x68000_keyboard_mouse/raw/7804e2cb25bb556c4f7c206e6624d596610cb27c/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x68000_keyboard_mouse/raw/7804e2cb25bb556c4f7c206e6624d596610cb27c/ExampleImage.jpg)
